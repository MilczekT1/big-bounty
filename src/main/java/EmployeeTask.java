import lombok.Data;

@Data
public class EmployeeTask {
    private Integer id;
    private String resultType;
    private String taskDescription;
    private Integer min;
    private Integer max;
    private Boolean activity;
    
    public EmployeeTask(String resultType, String taskDescription, Integer min, Integer max) {
        this.resultType = resultType;
        this.taskDescription = taskDescription;
        this.min = min;
        this.max = max;
        this.activity = true;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        
        EmployeeTask that = (EmployeeTask) o;
    
        return getId().equals(that.getId());
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getId().hashCode();
        return result;
    }
}

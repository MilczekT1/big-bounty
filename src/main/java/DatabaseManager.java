import lombok.Cleanup;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.List;

public class DatabaseManager {
    private static final String DB = "jdbc:mysql://5.135.218.27:3306/bigbounty";
    private static final String USER = "bbuser";
    private static final String USERPW = "BBgrawebowa1";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static Connection connection;
    private static DatabaseManager instance = new DatabaseManager();
    
    public static DatabaseManager getInstance(){
        return instance;
    }
    
    private DatabaseManager() {
        try {
            Class.forName(DRIVER).newInstance();
            connection = DriverManager.getConnection(DB, USER, USERPW);
            createTablesIfNotExists();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }
    
    private void createTablesIfNotExists() throws SQLException {
        @Cleanup
        Statement statement = connection.createStatement();
    
        try {
            Path path = Paths.get("src/main/resources/","Create.sql");
            List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));
            
            String query = "";
            for (String line : lines){
                query += line;
                if (line.endsWith(";")){
                    statement.execute(query);
                    query = "";
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void addEmployee(Employee employee) throws SQLException {
        String query = "INSERT INTO pracownicy(Imie, Nazwisko, Id_stanowiska, Id_przelozonego) VALUES(?, ?, ?, ?)";
    
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
    
        preStatement.setString(1, employee.getName());
        preStatement.setString(2, employee.getSurname());
        preStatement.setInt(3, employee.getPositionId());
        if (employee.getSuperiorId() > 0) {
            preStatement.setInt(4, employee.getSuperiorId());
        } else {
            preStatement.setNull(4, Types.NULL);
        }
        preStatement.execute();
    
        //TODO: log it
        System.out.println("added employee");
    }
    
    public static void addTask(EmployeeTask task) throws SQLException {
        String query = "INSERT INTO funkcje(Typ_wyniku, Tresc_zadania, MinPkt, MaxPkt, Czy_aktywne) VALUES(?, ?, ?, ?, ?)";
        
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
    
        preStatement.setString(1, task.getResultType());
        preStatement.setString(2, task.getTaskDescription());
        preStatement.setInt(3, task.getMin());
        preStatement.setInt(4, task.getMax());
        preStatement.setBoolean(5, task.getActivity());
        preStatement.execute();
    
        //TODO: log it
        System.out.println("added function");
    }
    
    public static void addExecutedTask(ExecutedTask exTask) throws SQLException{
        String query = "INSERT INTO wykonane_zadania(Id_funkcji, Id_pracownika, Wynik_zadania, Jednostka, Przyznane_punkty, Czas_rozpoczecia, Czas_zakonczenia, Czy_zaakceptowane) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
        
        preStatement.setInt(1, exTask.getEmployeeTaskId());
        preStatement.setInt(2, exTask.getEmployeeId());
        preStatement.setString(3, exTask.getTaskResult());
        
        if (exTask.getUnit().equals("none")) {
            preStatement.setNull(4, Types.NULL);
        } else {
            preStatement.setString(4, exTask.getUnit());
        }
        preStatement.setInt(5, exTask.getAssginedPoints());
        
        if (exTask.getStartTime().equals(exTask.getEndTime())){
            preStatement.setNull(6, Types.NULL);
            preStatement.setTimestamp(7, new Timestamp(exTask.getEndTime().getTime()));
        }
        else{
            preStatement.setTimestamp(6, new Timestamp(exTask.getStartTime().getTime()));
            preStatement.setTimestamp(7, new Timestamp(exTask.getEndTime().getTime()));
        }
        preStatement.setBoolean(8, exTask.getAccepted());
        preStatement.execute();
    
        //TODO: log it
        System.out.println("added executed task");
    }
    
    public static void addPosition(Position position) throws SQLException {
        String query = "INSERT INTO stanowiska(Nazwa) VALUES(?)";
    
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
    
        preStatement.setString(1, position.getName());
        preStatement.execute();
    
        //TODO: log it
        System.out.println("added function");
    }
    
    public static void deleteExecutedTaskWithID(Integer taskID) throws SQLException {
        String query = "DELETE FROM wykonane_zadania WHERE Id_zadania = ?";
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
        preStatement.setInt(1, taskID);
        preStatement.execute();
    
        //TODO: log it
        System.out.println("deleted executed task");
    }
    public static void deleteEmployeeWithID(Integer employeeID) throws SQLException {
        String query = "DELETE FROM pracownicy WHERE Id_pracownika = ?";
        
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
        preStatement.setInt(1, employeeID);
        preStatement.execute();
    
        //TODO: log it
        System.out.println("deleted employee");
    }
    public static void deleteTaskWithID(Integer taskID) throws SQLException {
        String query = "DELETE FROM funkcje WHERE Id_funkcji = ?";
        
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
        preStatement.setInt(1, taskID);
        preStatement.execute();
        
        //TODO: log it
        System.out.println("deleted task");
    }
    public static void deletePositionWithID(Integer positionID) throws SQLException {
        String query = "DELETE FROM stanowiska WHERE Id_stanowiska = ?";
        
        @Cleanup
        PreparedStatement preStatement = connection.prepareStatement(query);
        preStatement.setInt(1, positionID);
        preStatement.execute();
        
        //TODO: log it
        System.out.println("deleted position");
    }
}
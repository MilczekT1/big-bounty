import lombok.Data;

@Data
public class Position {
    private Integer positionId;
    private String name;
    
    public Position(String name) {
        this.name = name;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        
        Position position = (Position) o;
    
        return getPositionId().equals(position.getPositionId());
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getPositionId().hashCode();
        return result;
    }
}

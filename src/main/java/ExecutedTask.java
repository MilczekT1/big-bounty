import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;


@Data
public class ExecutedTask {
    private Integer executedTaskId;
    private Integer employeeTaskId;
    private Integer employeeId;
    private String taskResult;
    private String unit;
    private Integer assginedPoints;
    private Date startTime;
    private Date endTime;
    private Boolean accepted;
    
    public ExecutedTask(Integer employeeTaskId, Integer employeeId, String taskResult, String unit, Integer assginedPoints, Date startTime, Date endTime, Boolean accepted) {
        this.employeeTaskId = employeeTaskId;
        this.employeeId = employeeId;
        this.taskResult = taskResult;
        this.unit = unit;
        this.assginedPoints = assginedPoints;
        this.startTime = startTime;
        this.endTime = endTime;
        this.accepted = accepted;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        
        ExecutedTask that = (ExecutedTask) o;
    
        return getExecutedTaskId().equals(that.getExecutedTaskId());
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getExecutedTaskId().hashCode();
        return result;
    }
}

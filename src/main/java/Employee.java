import lombok.Data;

@Data
public class Employee {
    private int id;
    private String name;
    private String surname;
    private Integer positionId;
    private Integer superiorId;
    
    public Employee(String name, String surname, Integer positionId, Integer superiorId) {
        this.name = name;
        this.surname = surname;
        this.positionId = positionId;
        this.superiorId = superiorId;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        
        Employee employee = (Employee) o;
    
        return getId() == employee.getId();
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getId();
        return result;
    }
}

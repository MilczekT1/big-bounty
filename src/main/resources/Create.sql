-- WARNING: Do not make comment at lines with ;
CREATE TABLE IF NOT EXISTS stanowiska (
  Id_stanowiska INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nazwa varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS pracownicy (
  Id_pracownika INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Id_stanowiska INT NOT NULL,
  Id_przelozonego INT,
  Imie varchar(50) NOT NULL,
  Nazwisko varchar(50) NOT NULL,
  FOREIGN KEY (Id_stanowiska) REFERENCES stanowiska(Id_stanowiska),
  FOREIGN KEY (Id_przelozonego) REFERENCES pracownicy(Id_pracownika) ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS funkcje (
  Id_funkcji INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Typ_wyniku varchar(30) NOT NULL,
  Tresc_zadania varchar(40) NOT NULL,
  MinPkt INT NOT NULL DEFAULT 0,
  MaxPkt INT NOT NULL DEFAULT 0,
  Czy_aktywne TINYINT NOT NULL
);

CREATE TABLE IF NOT EXISTS wykonane_zadania (
  Id_zadania INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Id_funkcji INT NOT NULL,
  Id_pracownika INT,
  Wynik_zadania text NOT NULL,
  Jednostka varchar(20),
  Przyznane_punkty INT NOT NULL,
  Czas_zakonczenia TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  Czas_rozpoczecia TIMESTAMP,
  Czy_zaakceptowane TINYINT NOT NULL DEFAULT 0,

  FOREIGN KEY (Id_funkcji) REFERENCES funkcje(Id_funkcji) ON DELETE CASCADE,
  FOREIGN KEY (Id_pracownika) REFERENCES pracownicy(Id_pracownika) ON DELETE CASCADE
);